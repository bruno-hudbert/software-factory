# Software Factory

## Introduction

This is a software factory for the development of a software product. 
Make in order to learn setting and to get used to practice of good 
components in software development.## Requirements
just need to have docker and docker-compose installed

## Pre-requisites
Create a file named .env in the root directory of the project and add the following variables:
```
# General configuration
VOLUMES_DIRECTORY=/your/path/to/volumes

# Gitlab configuration
GITLAB_HTTP_PORT=9180
GITLAB_HTTPS_PORT=9443
GITLAB_SSH_PORT=42

# Jenkins configuration
JENKINS_PORT=9181
JENKINS_AGENT_PORT=50000

# Keycloak configuration
KEYCLOAK_PORT=9182
KEYCLOAK_SECURE_PORT=9444
KEYCLOAK_ADMIN_USERNAME=kc_admin
KEYCLOAK_ADMIN_PASSWORD=xxxxxxx
KEYCLOAK_USER_USERNAME=kc_user
KEYCLOAK_USER_PASSWORD=xxxxxxxxxx

# Keycloak database configuration
KEYCLOAK_DB_NAME=keycloak
KEYCLOAK_DB_PORT=5592
KEYCLOAK_DB_USER=keycloak
KEYCLOAK_DB_PASSWORD=xxxxxxxxxxxxx

# Hashicorp Vault configuration
VAULT_PORT=9183
VAULT_HIGH_AVAILABILITY_PORT=9201
VAULT_DEV_ROOT_TOKEN_ID=xxxxxxxxxxxxxxxxxxxxx

# Nexus configuration
NEXUS_PORT=9184
```


## How to use
Run the following command to start the factory:
```shell
docker-compose up -d
```
